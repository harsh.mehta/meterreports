from __future__ import unicode_literals
import frappe
from datetime import datetime, timedelta
from frappe.utils import flt, cint, cstr, today, nowdate, getdate, get_url

def validate(doc, method):

	import datetime
	t = datetime.date.today()

	for i in doc.customer_meter:
		if i.meter_id:
			if i.active:
				check_exist = frappe.db.get_value("Customer Connection History",{'parent':i.meter_id,"customer":doc.name},'name')
				if not check_exist:
					meter_customer = frappe.get_doc('Meter', i.meter_id)
					meter_customer.customer = doc.name
					meter_customer.owner = doc.name
					meter_customer.append("customer_history", {"customer":doc.name, "mobile_number":doc.mobile_no, "connection_date": t})
					meter_customer.save()
			else:
				check_exist = frappe.db.get_value("Customer Connection History",{'parent':i.meter_id,"customer":doc.name},'name')
				check_deactive = frappe.db.get_value("Customer Connection History",check_exist,"disconnection_date")
				if not check_deactive:
					frappe.db.set_value("Customer Connection History",check_exist,"disconnection_date", t)

@frappe.whitelist()
def get_inactive_meter(doctype, txt, searchfield, start, page_len, filters):

		return frappe.db.sql("""select name	from `tabMeter`
		where tabMeter.docstatus!=2 and {key} like %(txt)s
			and name not in (SELECT meter_id from `tabCUSTOMER METER` where active = 1)
		order by
			if(locate(%(_txt)s, name), locate(%(_txt)s, name), 99999),
			name
		limit %(start)s, %(page_len)s""".format(**{
			'key': searchfield
		}), {
			'txt': "%%%s%%" % txt,
			'_txt': txt.replace("%", ""),
			'start': start,
			'page_len': page_len
		})


@frappe.whitelist()
def get_inactive_meter1(doctype, txt, searchfield, start, page_len, filters):
    
    return frappe.db.sql("""select m.name as name from `tabMeter` m left outer join `tabCUSTOMER METER` cm on m.name = cm.meter_id  where cm.active= 0""")

@frappe.whitelist()
def get_meter_history(customer_name):
	meter_history = frappe.db.sql("""select * from `tabCustomer Connection History` where customer = %s ORDER BY connection_date DESC""" , (customer_name), as_dict = 1)
	
	html = ""	
	html += "<table style='border:1px solid #bbb'>"
	html += "<tr>"
	html += "<td style='padding:5px 10px;border:1px solid #bbb'>"
	html += "Meter ID"
	html += "</td>"
	html += "<td style='padding:5px 10px;border:1px solid #bbb'>"
	html += "Connection Date"
	html += "</td>"
	html += "<td style='padding:5px 10px;border:1px solid #bbb'>"
	html += "Disconnection Date"
	html += "</td>"
	html += "</tr>"
	if meter_history:
		for meter in meter_history:
			disconnection_date = '';
			connection_date = '';
			if meter.disconnection_date:
				disconnection_date = cstr(datetime.strptime(str(meter.disconnection_date), '%Y-%m-%d').strftime('%d-%m-%Y'))
			if meter.connection_date:
				connection_date = cstr(datetime.strptime(str(meter.connection_date), '%Y-%m-%d').strftime('%d-%m-%Y'))
			html += "<tr>"
			html += "<td style='padding:5px 10px;border:1px solid #bbb'><a href='"+frappe.utils.get_url()+"/desk#Form/Meter/"+meter.parent+"'>"
			html +=	meter.parent
			html += "</a></td>"
			html += "<td style='padding:5px 10px;border:1px solid #bbb'>"
			html +=	connection_date
			html += "</td>"
			html += "<td style='padding:5px 10px;border:1px solid #bbb'>"
			html +=	disconnection_date
			html += "</td>"
			html += "</tr>"
	html += "</table>"
	return html