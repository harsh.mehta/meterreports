from __future__ import unicode_literals
import frappe
from datetime import datetime, timedelta
from frappe.utils import flt, cint, cstr, today, nowdate
from frappe.model.naming import make_autoname

def meter_autoname(doc, method):
	if doc.area and doc.zone and doc.meter_type:
		type_code = ""
		if doc.meter_type == "SmartMeter":
			type_code = "S"
		elif doc.meter_type == "ManualMeter":
			type_code = "M"
		elif doc.meter_type == "WellMeter":
			type_code = "W"
		doc.name = make_autoname(cstr(doc.zone) +"-"+ cstr(doc.area) +"-" + cstr(type_code)+'-.##')

def validate(doc, method):
	pass
	# frappe.msgprint("hi")

@frappe.whitelist(allow_guest=True)
def make_open_function(meter_id):
	import requests
    # company = frappe.db.get_single_value("Global Defaults", "default_company")
    # api_endpoint = frappe.db.get_value("Company",company,"api_endpoint")
    # url = str(api_endpoint)+str(meter_id)+"/open"
	url = "http://202.130.44.124:38080/smart-meter-api/api/collector"
	data={"key": 123456,"logKey": 1234568,"latitude": "sample latitude","longitude": "sample longitude"}
	headers = {'content-type': 'application/json'}
	r = requests.post( url, data =data, headers=headers)
	return  r.json()


@frappe.whitelist(allow_guest=True)
def make_close_function(meter_id):
	import requests

	company = frappe.db.get_single_value("Global Defaults", "default_company")
	api_endpoint = frappe.db.get_value("Company",company,"api_endpoint")

    url = str(api_endpoint)+str(meter_id)+"/close"
	# url = "http://202.130.44.124:38080/smart-meter-api/api/collector"
	#  data={"key": 123456,"logKey": 1234568,"latitude": "sample latitude","longitude": "sample longitude"}
	headers = {'content-type': 'application/json'}
	r = requests.post( url, data =data, headers=headers)
	return  r.json()

@frappe.whitelist()
def make_open_function(meter_id):
	import requests
	result = None
	try:
		r = requests.get("http://202.130.44.124:38080/smart-meter-api/api/meter/"+cstr(meter_id)+"/open")
		if r.status_code is not 200:
			raise Exception
		else:
			result = r.json()
			frappe.msgprint("Meter SUcuuessfully Open")
			suc = frappe.get_doc(dict(
				doctype = 'Communication',
				subject="API call Sucess.",
				communication_type = 'Comment',
				content=cstr(result),
				reference_doctype='Meter',
				reference_name=meter_id
				)).insert(ignore_permissions=True)
	except requests.Timeout:
		frappe.msgprint('Request timed out.')
		err = frappe.get_doc(dict(
			doctype = 'Communication',
			subject="API Error: Request timed out",
			communication_type = 'Comment',
			content="API Error: Request timed out",
			reference_doctype='Meter',
			reference_name=meter_id
		)).insert(ignore_permissions=True)
	except requests.ConnectionError:
		frappe.msgprint('API connection error.')
		err = frappe.get_doc(dict(
			doctype = 'Communication',
			subject="API Error: API connection error.",
			communication_type = 'Comment',
			content="API Error: API connection error",
			reference_doctype='Meter',
			reference_name=meter_id
			)).insert(ignore_permissions=True)
	except requests.HTTPError:
		frappe.msgprint('An HTTP error occurred.')
		err = frappe.get_doc(dict(
			doctype = 'Communication',
			subject="An HTTP error occurred.",
			communication_type = 'Comment',
			content="API Error: An HTTP error occurred.",
			reference_doctype='Meter',
			reference_name=meter_id
			)).insert(ignore_permissions=True)
	except Exception:
		frappe.msgprint('An unexpected error occurred.')
		err = frappe.get_doc(dict(
			doctype = 'Communication',
			subject="An unexpected error occurred",
			communication_type = 'Comment',
			content="API Error: An unexpected error occurred",
			reference_doctype='Meter',
			reference_name=meter_id
			)).insert(ignore_permissions=True)
	return result

@frappe.whitelist()
def make_close_function(meter_id):
	import requests
	result = None
	try:
		r = requests.get("http://202.130.44.124:38080/smart-meter-api/api/meter/"+cstr(meter_id)+"/close")
		if r.status_code is not 200:
			raise Exception
		else:
			result = r.json()
			frappe.msgprint("Meter SUcuuessfully Close")
			suc = frappe.get_doc(dict(
				doctype = 'Communication',
				subject="API call Sucess.",
				communication_type = 'Comment',
				content=cstr(result),
				reference_doctype='Meter',
				reference_name=meter_id
				)).insert(ignore_permissions=True)
	except requests.Timeout:
		frappe.msgprint('Request timed out.')
		err = frappe.get_doc(dict(
			doctype = 'Communication',
			subject="API Error: Request timed out",
			communication_type = 'Comment',
			content="API Error: Request timed out",
			reference_doctype='Meter',
			reference_name=meter_id
		)).insert(ignore_permissions=True)
	except requests.ConnectionError:
		frappe.msgprint('API connection error.')
		err = frappe.get_doc(dict(
			doctype = 'Communication',
			subject="API Error: API connection error.",
			communication_type = 'Comment',
			content="API Error: API connection error",
			reference_doctype='Meter',
			reference_name=meter_id
			)).insert(ignore_permissions=True)
	except requests.HTTPError:
		frappe.msgprint('An HTTP error occurred.')
		err = frappe.get_doc(dict(
			doctype = 'Communication',
			subject="An HTTP error occurred.",
			communication_type = 'Comment',
			content="API Error: An HTTP error occurred.",
			reference_doctype='Meter',
			reference_name=meter_id
			)).insert(ignore_permissions=True)
	except Exception:
		frappe.msgprint('An unexpected error occurred.')
		err = frappe.get_doc(dict(
			doctype = 'Communication',
			subject="An unexpected error occurred",
			communication_type = 'Comment',
			content="API Error: An unexpected error occurred",
			reference_doctype='Meter',
			reference_name=meter_id
			)).insert(ignore_permissions=True)
	return result
