# Copyright (c) 2013, August Infotech and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.utils import flt, cstr
from frappe import msgprint, _


def execute(filters=None):
	if not filters: filters = {}
	columns, data = [], []
	columns = get_columns(filters)
	# customer_data = get_customer_data(filters)

	# for c in customer_data:
	# 	cust_group = frappe.db.get_value("Customer",c.customer,"customer_group") or ""
	# 	cust_name = frappe.db.get_value("Customer",c.customer,"customer_name") or ""
	# 	cust_phone = frappe.db.get_value("Customer",c.customer,"phone") or ""
	# 	outstanding_amount = 0
	# 	if c.sales_invoice:
	# 		outstanding_amount = frappe.db.get_value("Sales Invoice",c.sales_invoice,"outstanding_amount")
	# 	reader_name = ""
	# 	if c.meter_reader:
	# 		reader_emp_name = frappe.db.get_value("Employee",c.meter_reader,"employee_name")
	# 		reader_name = cstr(reader_emp_name) + "(" + cstr(c.meter_reader) + ")"
	# 	row = [c.zone, c.area,c.customer, "Connected", cstr(cust_name),
	# 	 cstr(cust_group),c.name,reader_name , cstr(cust_phone),c.sales_invoice,outstanding_amount]
	# 	data.append(row)
	inv_data = get_inv_data(filters)

	for i in inv_data:
		cust_group = frappe.db.get_value("Customer",i.customer,"customer_group") or ""
		cust_name = frappe.db.get_value("Customer",i.customer,"customer_name") or ""
		cust_phone = frappe.db.get_value("Customer",i.customer,"phone") or ""
		m_zone = frappe.db.get_value("Meter",i.meter,"zone") or ""
		m_area = frappe.db.get_value("Meter",i.meter,"area") or ""
		reader_name = ""
		if i.meter_reader:
			reader_emp_name = frappe.db.get_value("Employee",i.meter_reader,"employee_name")
			reader_name = cstr(reader_emp_name) + "(" + cstr(i.meter_reader) + ")"
		row = [m_zone,m_area,i.customer, "Connected", cust_name,cust_group,i.meter,
		reader_name, cust_phone, i.invoice, i.rounded_total, i.outstanding_amount or 0]
		data.append(row)
	return columns, data

def get_columns(filters):
	columns = [
			_("Zone") + ":Link/Zone:80",
			_("Area") + ":Link/Area:120",
			_("Customer Id") + ":Link/Customer:120",
			_("Status") + "::120",
			_("Customer Name") + "::120",
			_("Customer Group") + ":Link/Customer Group:120",
			_("Flow Meter") + ":Link/Meter:120",
			_("Meter Reader") + "::120",
			_("Phone Number") + "::120",
			_("Invoice") + ":Link/Sales Invoice:120",
			_("Invoiced Amt") + ":Currency:120",
			_("Balance") + ":Currency:120",

		]
	return columns

def get_customer_data(filters):
	pass
	# conditions = get_conditions(filters)
	# if conditions:
		# customer_data = frappe.db.sql("""SELECT * from `tabMETER READ` where docstatus < 2 %s Order By read_date
		# """% conditions, filters,as_dict=1)
	# customer_data = frappe.db.sql("""SELECT t2.customer, t2.meter_reader, t2.sales_invoice, t1.name, t1.zone, t1.area
	# from `tabMETER READ` t2, `tabMeter` t1 where t1.name = t2.parent and t2.docstatus < 2 %s Order By t2.read_date desc
	# """% conditions, filters,as_dict=1)
	# customer_data = frappe.db.sql("""SELECT t2.customer, t2.meter_reader, t2.sales_invoice, t1.name, t1.zone, t1.area
	# from `tabMETER READ` t2, `tabMeter` t1 where t1.name = t2.parent and t2.docstatus < 2
	# """,(),as_dict=1)
	# customer_data = []
	# return customer_data

def get_conditions(filters):
	conditions = ""
	if filters.get("customer"): conditions += " and t1.customer = %(customer)s"

	return conditions


def get_inv_data(filters):
	conditions = get_conditions(filters)

	in_data = frappe.db.sql("""SELECT t1.customer, t1.rounded_total, t1.outstanding_amount, t1.name as invoice, t2.parent as meter, t2.meter_reader
	from `tabSales Invoice` t1, `tabMETER READ` t2 where t1.name = t2.sales_invoice %s Order By t1.posting_date desc"""% conditions, filters,as_dict=1)

	return in_data
