# Copyright (c) 2013, August Infotech and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.utils import flt, cstr
from frappe import msgprint, _


def execute(filters=None):
	if not filters: filters = {}
	columns, data = [], []
	columns = get_columns(filters)
	# customer_data = get_customer_data(filters)

	# for c in customer_data:
	# 	cust_group = frappe.db.get_value("Customer",c.customer,"customer_group") or ""
	# 	cust_name = frappe.db.get_value("Customer",c.customer,"customer_name") or ""
	# 	cust_phone = frappe.db.get_value("Customer",c.customer,"phone") or ""
	# 	outstanding_amount = 0
	# 	if c.sales_invoice:
	# 		outstanding_amount = frappe.db.get_value("Sales Invoice",c.sales_invoice,"outstanding_amount")
	# 	reader_name = ""
	# 	if c.meter_reader:
	# 		reader_emp_name = frappe.db.get_value("Employee",c.meter_reader,"employee_name")
	# 		reader_name = cstr(reader_emp_name) + "(" + cstr(c.meter_reader) + ")"
	# 	row = [c.zone, c.area,c.customer, "Connected", cstr(cust_name),
	# 	 cstr(cust_group),c.name,reader_name , cstr(cust_phone),c.sales_invoice,outstanding_amount]
	# 	data.append(row)
	inv_data = get_inv_data(filters)

	for i in inv_data:
		cust_group = frappe.db.get_value("Customer",i.customer,"customer_group") or ""
		cust_name = frappe.db.get_value("Customer",i.customer,"customer_name") or ""
		cust_phone = frappe.db.get_value("Customer",i.customer,"phone") or ""
		m_zone = frappe.db.get_value("Meter",i.meter,"zone") or ""
		m_area = frappe.db.get_value("Meter",i.meter,"area") or ""
		serial_no = frappe.db.get_value("Meter",i.meter,"key") or ""
		reader_name = ""
		if i.meter_reader:
			reader_emp_name = frappe.db.get_value("Employee",i.meter_reader,"employee_name")
			reader_name = cstr(reader_emp_name) + "(" + cstr(i.meter_reader) + ")"

		meter_det = frappe.db.sql("""SELECT previous_reading, reading, total_units_consumed, read_date from `tabMETER READ` where sales_invoice = %s limit 1""",( i.invoice),as_dict=1)
		if meter_det:
			meter_det = meter_det[0]

		cus_id = i.meter.rsplit('-')
		if len(cus_id)>1:
			cus_id = cus_id[1]
		else:
			cus_id = i.customer
		pay_entry_name = frappe.db.get_value("Payment Entry Reference", {"reference_doctype":"Sales Invoice","reference_name":i.invoice},
		"parent") or ""
		pay_entry_amt = frappe.db.get_value("Payment Entry Reference", {"reference_doctype":"Sales Invoice","reference_name":i.invoice},
		"allocated_amount") or 0
		pay_status = ""
		if i.outstanding_amount > 0:
			pay_status = "Unpaid"
		else:
			pay_status = "Paid"
		row = [m_zone,m_area,i.customer,cust_phone,cust_group,i.meter,serial_no,frappe.utils.data.getdate(meter_det.read_date),
		meter_det.previous_reading, meter_det.reading,meter_det.total_units_consumed,
		i.owner,  i.invoice, pay_status,frappe.utils.data.getdate(i.posting_date), i.rounded_total,pay_entry_name, pay_entry_amt,i.outstanding_amount or 0 ]
		data.append(row)
	return columns, data

def get_columns(filters):
	columns = [
			_("Zone") + ":Link/Zone:50",
			_("Area") + ":Link/Area:50",
			_("Customer") + ":Link/Customer:150",
			_("Phone Number") + "::100",
			_("Customer Group") + ":Link/Customer Group:100",
			_("Supply-Name") + ":Link/Meter:100",
			_("Flow Meter") + "::80",
			_("Reading Date") + ":Date:100",
			_("P. Reading") + "::100",
			_("C. Reading") + "::100",
			_("Units Consumed") + "::80",
			_("Created By") + "::150",
			_("Invoice") + ":Link/Sales Invoice:80",
			_("Status") + "::50",
			_("Invoice Date") + ":Date:80",
			_("Invoiced Amt") + ":Currency:100",
			_("Payment Entry") + ":Link/Payment Entry:100",
			_("Paid Amt") + ":Currency:100",
			_("Outstanding Amt") + ":Currency:100"

		]
	return columns

def get_customer_data(filters):
	pass
	# conditions = get_conditions(filters)
	# if conditions:
		# customer_data = frappe.db.sql("""SELECT * from `tabMETER READ` where docstatus < 2 %s Order By read_date
		# """% conditions, filters,as_dict=1)
	# customer_data = frappe.db.sql("""SELECT t2.customer, t2.meter_reader, t2.sales_invoice, t1.name, t1.zone, t1.area
	# from `tabMETER READ` t2, `tabMeter` t1 where t1.name = t2.parent and t2.docstatus < 2 %s Order By t2.read_date desc
	# """% conditions, filters,as_dict=1)
	# customer_data = frappe.db.sql("""SELECT t2.customer, t2.meter_reader, t2.sales_invoice, t1.name, t1.zone, t1.area
	# from `tabMETER READ` t2, `tabMeter` t1 where t1.name = t2.parent and t2.docstatus < 2
	# """,(),as_dict=1)
	# customer_data = []
	# return customer_data

def get_conditions(filters):
	conditions = ""
	if filters.get("customer"): conditions += " and t1.customer = %(customer)s"
	if filters.get("meter"): conditions += " and t2.parent = %(meter)s"
	if filters.get("from_date"): conditions += " and t1.posting_date >= %(from_date)s"
	if filters.get("to_date"): conditions += " and t1.posting_date <= %(to_date)s"
	return conditions


def get_inv_data(filters):
	conditions = get_conditions(filters)

	in_data = frappe.db.sql("""SELECT t1.customer, t1.owner , t1.rounded_total, t1.outstanding_amount, t1.name as invoice, t2.parent as meter, t2.meter_reader, t1.posting_date
	from `tabSales Invoice` t1, `tabMETER READ` t2 where t1.name = t2.sales_invoice %s Order By t1.posting_date desc"""% conditions, filters,as_dict=1)

	return in_data
