// Copyright (c) 2016, August Infotech and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Water Customer"] = {
	"filters": [
		{
			"fieldname":"customer",
			"label": __("Customer"),
			"fieldtype": "Link",
			"options": "Customer",
			},
			{
			"fieldname":"meter",
			"label": __("Meter"),
			"fieldtype": "Link",
			"options": "Meter",
			},
			{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			// "options": "Meter",
			},
			{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			// "options": "Meter",
			},
	],

	"formatter":function (row, cell, value, columnDef, dataContext, default_formatter) {
		value = default_formatter(row, cell, value, columnDef, dataContext);
		if (columnDef.id=="Status" && dataContext['Status'] == "Unpaid") {
			value = "<span style='color:Red'>" + value + "</span>";
		}
		if (columnDef.id=="Status" && dataContext['Status'] == "Paid") {
			value = "<span style='color:Green'>" + value + "</span>";
		}
		return value;
	}		
}
