# Copyright (c) 2013, August Infotech and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.utils import flt
from frappe import msgprint, _


def execute(filters=None):
	columns, data = [], []
	columns = get_columns(filters)
	usage_data = get_usage_data(filters)
	well_data = get_well_data(filters)
	total_smart_meter = 0
	total_manual_meter = 0
	total_well_meter = 0
	if usage_data:
		for c in usage_data:
			row = [c.area, c.zone, c.customer_group, c.meter_type, c.total_units_consumed]
			if c.meter_type == "SmartMeter":
				total_smart_meter += flt(c.total_units_consumed)
			elif c.meter_type == "ManualMeter":
				total_manual_meter += flt(c.total_units_consumed)
			data.append(row)
		data.append([""])
	if well_data:
		for c in well_data:
			row = [c.area, c.zone, c.customer_group, c.meter_type, c.total_units_consumed]
			total_well_meter += flt(c.total_units_consumed)
			data.append(row)
		data.append([""])
	row1 = ["", "", "Total", "SmartMeter", total_smart_meter]
	row2 = ["", "", "Total", "ManualMeter", total_manual_meter]
	row3 = ["", "", "Total", "WellMeter", total_well_meter]
	row4 = ["", "", "Reading Difference", "", flt(total_well_meter)-flt(flt(total_smart_meter)+flt(total_manual_meter))]

	data.append(row1)
	data.append(row2)
	data.append(row3)
	data.append(row4)

	return columns, data

def get_columns(filters):
	columns = [
			_("Area") + ":Link/Area:120",
			_("Zone") + ":Link/Zone:80",
			_("Customer Group") + ":Link/Customer Group:120",
			_("Meter Type") + "::120",
			_("Total Units Consumed") + ":Float:120",
			]
	return columns

def get_usage_data(filters):
	conditions = get_conditions(filters, meter_type="meter")
	
	customer_data = frappe.db.sql("""SELECT t1.zone, t1.area, t1.meter_type, t3.customer_group,
	 SUM(t2.total_units_consumed) as total_units_consumed
	FROM `tabMeter` t1, `tabMETER READ` t2, `tabCustomer` t3
	where t1.name =t2.parent and t2.customer = t3.name %s
	"""%
	conditions, filters,as_dict=1)
	return customer_data

def get_well_data(filters):
	conditions = get_conditions(filters, meter_type="well")

	well_data = frappe.db.sql("""SELECT t1.zone, t1.area, t1.meter_type, t3.customer_group,
	 SUM(t2.total_units_consumed) as total_units_consumed
	FROM `tabMeter` t1, `tabMETER READ` t2, `tabCustomer` t3
	where t1.name =t2.parent and t1.meter_type = 'WellMeter' and t2.customer = t3.name %s
	"""%
	conditions, filters,as_dict=1)
	return well_data

def get_conditions(filters, meter_type=None):
	if not filters.get("smart_meter") and not filters.get("manual_meter") :
		frappe.msgprint(str("Atleast select one meter type"))

	conditions = ""
	if filters.get("from_date"): conditions += " and t2.read_date >= %(from_date)s"
	if filters.get("to_date"): conditions += " and t2.read_date <= %(to_date)s"
	if meter_type and meter_type == "meter":
		if filters.get("smart_meter") and filters.get("manual_meter"):
			conditions += " and t1.meter_type != 'WellMeter'"
		elif filters.get("smart_meter") and not filters.get("manual_meter"):
			conditions += " and t1.meter_type = 'SmartMeter'"
		elif not filters.get("smart_meter") and filters.get("manual_meter"):
			conditions += " and t1.meter_type = 'ManualMeter'"


	conditions += " Group by "
	grp_list = []
	if filters.get("area"):
		grp_list.append("t1.area")
	if filters.get("zone"):
		grp_list.append("t1.zone")
	if filters.get("customer_group"):
		grp_list.append("t3.customer_group")
	grp_list.append("t1.meter_type")
	conditions += "{0}".format(", ".join(grp_list))
	# frappe.msgprint(str(conditions))
	return conditions
