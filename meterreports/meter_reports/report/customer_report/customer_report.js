// Copyright (c) 2016, August Infotech and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Customer Report"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.add_months(frappe.datetime.get_today(), -1),
			"width": "80"
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today()
		},
		{
			"fieldname":"area",
			"label": __("Group By Area"),
			"fieldtype": "Check"			
		},
		{
			"fieldname":"zone",
			"label": __("Group By Zone"),
			"fieldtype": "Check"			
		},
		{
			"fieldname":"customer_group",
			"label": __("Group By Customer Group"),
			"fieldtype": "Check"			
		},
	]
}
