# Copyright (c) 2013, August Infotech and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.utils import flt
from frappe import msgprint, _

def execute(filters=None):
	columns, data = [], []
	columns = get_columns(filters)
	customer_data = get_customer_data(filters)
	if customer_data:
		for c in customer_data:
			paid_amount =  flt(flt(c.rounded_total)-flt(c.outstanding_amount))
			row = [c.area, c.zone, c.customer_group, c.meter_type, c.outstanding_amount,paid_amount or 0, c.total_units_consumed]
			data.append(row)
			# frappe.msgprint(str(c))
	return columns, data

def get_columns(filters):
	columns = [
			_("Area") + ":Link/Area:120",
			_("Zone") + ":Link/Zone:80",
			_("Customer Group") + ":Link/Customer Group:120",
			_("Meter Type") + "::120",
			_("Outstanding Amount") + ":Currency:120",
			_("Total Payment Amount") + ":Currency:120",
			_("Total Units Consumed") + ":Float:120",

		]
	return columns

def get_customer_data(filters):
	conditions = get_conditions(filters)
	if conditions:
		customer_data = frappe.db.sql("""SELECT t1.zone, t1.area, t1.meter_type, t3.customer_group,
		 SUM(t2.total_units_consumed) as total_units_consumed, SUM(t4.outstanding_amount) as outstanding_amount,
		 SUM(t4.rounded_total) as rounded_total
		FROM `tabMeter` t1, `tabMETER READ` t2, `tabCustomer` t3, `tabSales Invoice` t4
		where t1.name =t2.parent and t1.meter_type != 'WellMeter' and t2.customer = t3.name and t2.sales_invoice = t4.name %s
		"""%
		conditions, filters,as_dict=1)
	return customer_data

def get_conditions(filters):
	conditions = ""
	if filters.get("from_date"): conditions += " and t2.read_date >= %(from_date)s"
	if filters.get("to_date"): conditions += " and t2.read_date <= %(to_date)s"
	conditions += " Group by "
	grp_list = []
	if filters.get("area"):
		grp_list.append("t1.area")
	if filters.get("zone"):
		grp_list.append("t1.zone")
	if filters.get("customer_group"):
		grp_list.append("t3.customer_group")
	grp_list.append("t1.meter_type")
	conditions += "{0}".format(", ".join(grp_list))
	# frappe.msgprint(str(conditions))
	return conditions
