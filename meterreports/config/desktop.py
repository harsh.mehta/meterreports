# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Meter Reports",
			"color": "#D10056",
			"icon": "octicon octicon-ruby",
			"type": "module",
			"label": _("Meter Reports")
		}
	]
