cur_frm.fields_dict['customer_meter'].grid.get_field("meter_id").get_query = function(doc, dt, dn)
{
	return {
    query: "meterreports.meter_reports.customer.get_inactive_meter",
		filters: {
			// 'meter_type': "Smart",
		}
	}
}

frappe.ui.form.on("Customer", {
	onload: function(frm) {
		frappe.call({
			method: "meterreports.meter_reports.customer.get_meter_history",
			args: {
				"customer_name": cur_frm.doc.name,
			},
			callback: function (r) {
				if (r.message){
					$(frm.fields_dict.customer_meter_history.wrapper).html(r.message)
				}
			}
		});
	}
});