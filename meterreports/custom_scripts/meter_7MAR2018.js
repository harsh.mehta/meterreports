frappe.ui.form.on('Meter', {
	refresh: function(frm) {

	// frappe.ui.form.on("METER READ", "meter_read_add", function(frm, cdt, cdn){
	//
	// 	frappe.utils.filter_dict(cur_frm.fields_dict["meter_read"].grid.grid_rows_by_docname[cdn].docfields, {"fieldname": "previous_reading"})[0].read_only = true;
	// 	cur_frm.fields_dict["meter_read"].grid.grid_rows_by_docname[cdn].fields_dict["previous_reading"].refresh();
	//
	// })
	// frappe.ui.form.on("METER READ", "meter_read_add", function(frm, cdt, cdn){
	// 	if(frm.get_field('meter_read').grid.fields_map.idx) {
	// 		frm.fields_dict.meter_read.grid.toggle_enable("previous_reading",
	// 			(meter_read.idx != 1)? true: false);
	// 	}
 // 	})
	if(!frm.doc.__islocal) {
			cur_frm.add_custom_button(__('Open'), cur_frm.cscript.make_open_function).addClass("btn-primary");
		}
		if(!frm.doc.__islocal) {
				cur_frm.add_custom_button(__('Close'), cur_frm.cscript.make_close_function).addClass("btn-primary");
			}
	},
	make_open_function: function(frm){
		frappe.call({
			type:"GET",
			method: "meterreports.meter_reports.customer.make_open_function",
			args:{
				meter_id :frm.doc.name
			},
			callback: function(r){
				console.log(r)
			}
		})
	},
	make_close_function: function(frm){
		frappe.call({
			type:"GET",
			method: "meterreports.meter_reports.customer.make_close_function",
			args:{
				meter_id :frm.doc.name
			},
			callback: function(r){
					console.log(r)
			}
		})
	}
});
