# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "meterreports"
app_title = "Meter Reports"
app_publisher = "August Infotech"
app_description = "Provide Different Reports For Meter"
app_icon = "octicon octicon-ruby"
app_color = "#D10056"
app_email = "info@augustinfotech.com"
app_license = "GNU General Public Licence"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
app_include_css = "/assets/meterreports/css/meterreports.css"
# app_include_js = "/assets/meterreports/js/meterreports.js"

# include js, css files in header of web template
# web_include_css = "/assets/meterreports/css/meterreports.css"
# web_include_js = "/assets/meterreports/js/meterreports.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
doctype_js = {
			"Customer" : ["custom_scripts/customer.js"],
			"Meter" : ["custom_scripts/meter.js"]
			}


# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "meterreports.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "meterreports.install.before_install"
# after_install = "meterreports.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "meterreports.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

#doc_events = {
#    "Customer" :{
#        "validate" : "meterreports.meter_reports.customer.validate",
#    },
#    "Meter" :{
#        "validate" : "meterreports.meter_reports.meter.validate",
#        "autoname" : "meterreports.meter_reports.meter.meter_autoname",
#    }
#}

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"meterreports.tasks.all"
# 	],
# 	"daily": [
# 		"meterreports.tasks.daily"
# 	],
# 	"hourly": [
# 		"meterreports.tasks.hourly"
# 	],
# 	"weekly": [
# 		"meterreports.tasks.weekly"
# 	]
# 	"monthly": [
# 		"meterreports.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "meterreports.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "meterreports.event.get_events"
# }
